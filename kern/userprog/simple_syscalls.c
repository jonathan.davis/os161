#include <syscall.h>
#include <types.h>
#include <lib.h>
#include <thread.h>


int sys__exit(int code)
{
    kprintf("exited with code %d.\n", code);
    thread_exit();

    return code;
}


/* Prints input param c
 * 
 * Return: 0 if c divisible by 5, 1 otherwise
 */
int sys_printint(int c)
{
    kprintf("%d\n", c);

    if (c % 5 == 0) {
        return 0;
    } else {
        return 1;
    }
}


/* Prints the reverse of a string
 *
 * Return: 0 if string length is divisible by 3, 1 otherwise
 */
int sys_reversestring(const char *str, int len)
{
    int i;
    for (i = len-1; i >= 0; --i) {
        kprintf("%c", str[i]);
    }
    kprintf("\n");

    if (len % 3 == 0) {
        return 0;
    } else {
        return 1;
    }
}

