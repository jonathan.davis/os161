/*
 * Simple program to test sys__exit.
 *
 */

#include <stdlib.h>

int main(int argc, char *argv[])
{
    exit(0);
}
