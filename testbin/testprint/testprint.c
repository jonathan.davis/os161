/*
 * Simple program to test sys_printint.
 *
 */

#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    int i;
    for (i = 1; i <= 5; ++i) {
        printint(i);
    }

    exit(0);
}
