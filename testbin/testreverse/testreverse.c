/*
 * Simple program to test sys_reversestring.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    const char *str = "Hello, world!";
    reversestring(str, strlen(str));

    exit(0);
}
